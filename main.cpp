#include "./includes.h"

class Game
{
public:
    GLFWwindow * window;

    void init()
    {
        glfwInit();

        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
        glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

        window = glfwCreateWindow(800, 600, "Test", nullptr, nullptr);

        glfwMakeContextCurrent(window);
        


        glewExperimental = GL_TRUE;
        glewInit();



        auto vertShader = compileGlslCode("./basic.vert", GL_VERTEX_SHADER);
        auto fragShader = compileGlslCode("./basic.frag", GL_FRAGMENT_SHADER);


        shader = glCreateProgram();
        glAttachShader(shader, vertShader);
        glAttachShader(shader, fragShader);
        glLinkProgram(shader);
        glDeleteShader(vertShader);
        glDeleteShader(fragShader);
    }

    void loop()
    {
        glfwSwapBuffers(window);
        glfwPollEvents();

        if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        {glfwSetWindowShouldClose(window, GL_TRUE);}



        float verts[] = 
        {
            0.5f, 0.5f, 0.0f,
            0.5f, -0.5f, 0.0f,
            -0.5f, -0.5f, 0.0f,
            -0.5f, 0.5f, 0.0f
        };
        GLuint index[] = 
        {
            0, 1, 3,
            1, 2, 3
        };

        GLuint vertBuffer, vertArray, vertIndex;
        glGenBuffers(1, &vertBuffer);
        glGenVertexArrays(1, &vertArray);
        glGenBuffers(1, &vertIndex);

        glBindVertexArray(vertArray);

        glBindBuffer(GL_ARRAY_BUFFER, vertBuffer);
        glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertIndex);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(index), index, GL_STATIC_DRAW);

        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(float), nullptr);
        glEnableVertexAttribArray(0);

        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glUseProgram(shader);
        glBindVertexArray(vertArray);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
        glBindVertexArray(0);
    }

    void terminate()
    {
        glfwTerminate();
    }

private:
    
    GLuint shader;

    GLuint compileGlslCode(char const* file, GLenum TYPE)
    {
        std::ifstream glslCompCodeIn(file);
        std::string glslCompCode((std::istreambuf_iterator<char>(glslCompCodeIn)), 
                                  std::istreambuf_iterator<char>());

        const char* c = glslCompCode.c_str();
        GLuint compShader = glCreateShader(TYPE);
        glShaderSource(compShader, 1, &(c), nullptr);
        delete c;
        glCompileShader(compShader);

        GLint isSuccess;
        glGetShaderiv(compShader, GL_COMPILE_STATUS, &isSuccess);

        if(!isSuccess)
        {
            char error[512];

            glGetShaderInfoLog(compShader, 512, nullptr, error);
            std::cout << error << "\n";
        }


        return compShader;
    }
};


int main()
{
	Game game;

	game.init();

    do
    {
	    game.loop();
    }
    while(!glfwWindowShouldClose(game.window));

	game.terminate();	
	
	return 0;
}
